#!/usr/bin/python

# test_shell.py

# Framework for testing shell-like interactive programs using python's unittest module

import fcntl
import os
import subprocess
from time import sleep
import unittest


# Configuration

# Shell to test
SHELL = './shellC'

SHELL_HISTORY_NAME = 'historyFile.txt'

# Possible locations of the shell's history file
SHELL_HISTORY_LOCATIONS = [
    os.path.join(os.environ['HOME'], SHELL_HISTORY_NAME),
    os.path.join(os.getcwd(), SHELL_HISTORY_NAME),
]

# Number of seconds to wait before trying to read the shell's output
SHELL_IO_TIMEOUT = 0.05


def cleanup():
    """Cleans up history file used by shell"""

    # Sleep so the subprocess has time for IO
    sleep(SHELL_IO_TIMEOUT)

    for histfile in SHELL_HISTORY_LOCATIONS:
        try:
            os.remove(histfile)
        except OSError:
            # The history file wasn't there
            # Not a big deal since there's a test case for this
            pass


def set_nonblock(fd):
    """Sets a file descriptor as non-blocking"""

    flags = fcntl.fcntl(fd, fcntl.F_GETFL)
    flags = flags | os.O_NONBLOCK
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)


class ShellTestCase(unittest.TestCase):

    def setUp(self):
        self.start_shell()

    def tearDown(self):
        self.stop_shell()
        cleanup()

    def start_shell(self):
        """Starts the shell with proper parameters and strips the leading prompt"""

        self.shell = subprocess.Popen(SHELL,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            bufsize=1)

        set_nonblock(self.shell.stdin)
        set_nonblock(self.shell.stdout)

        sleep(SHELL_IO_TIMEOUT)

        self.prompt = self.shell.stdout.read()

    def stop_shell(self):
        """Stops the shell by closing its stdin (thus causing EOF)"""

        self.shell.stdin.close()

        sleep(SHELL_IO_TIMEOUT)

        self.shell.kill()

    def restart(self):
        """Restarts the shell but preserves history by not performing cleanup"""
        self.stop_shell()
        self.start_shell()

    def execute_command(self, command):
        """Executes a command in the shell and returns its literal output"""

        self.shell.stdin.write("%s\n" % (command,))

        sleep(SHELL_IO_TIMEOUT)

        # This part is a bit dodgy... is there a way to poll a non-blocking FD?
        while True:
            try:
                output = self.shell.stdout.read()
                break
            except IOError:
                continue

        # Strip prompts and leading/trailing whitespace before returning output
        if output.startswith(self.prompt):
            output = output[len(self.prompt):]
        if output.endswith(self.prompt):
            output = output[:-len(self.prompt)]
        return output.strip()


class HistoryFileIoTestCase(ShellTestCase):

    def test_creates_history_file(self):
        """Test whether a history file is created after exiting the shell"""

        self.execute_command("echo 1")
        self.restart()

        self.assertTrue(os.path.isfile(SHELL_HISTORY_LOCATIONS[0]) or
                        os.path.isfile(SHELL_HISTORY_LOCATIONS[1]))

        if os.path.isfile(SHELL_HISTORY_LOCATIONS[1]):
            print "WARNING: file is saved in CWD, not $HOME"

    def test_history_outputs_correctly(self):
        """Manually prints history file contents to console, then passes"""

        self.execute_command("echo foo")
        self.execute_command("echo bar")

        self.restart()

        histfile = [f for f in SHELL_HISTORY_LOCATIONS if os.path.exists(f)][0]

        with open(histfile, 'r') as f:
            # throws exception w/ encrypted history
            print f.read()

    def test_history_inputs_correctly(self):
        """Tests that history is loaded into the shell correctly"""

        self.execute_command("echo foo")
        self.execute_command("echo bar")
        self.execute_command("echo baz")

        self.restart()

        lines = self.execute_command("history").split('\n')

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        self.assertGreater(len(lines), 0)

        self.assertIn("echo foo", lines[0])
        self.assertIn("echo bar", lines[1])
        self.assertIn("echo baz", lines[2])

    def test_commands_correctly_numbered(self):
        """Tests that history is loaded into the shell and numbered correctly"""

        self.execute_command("echo foo")
        self.execute_command("echo bar")
        self.execute_command("echo baz")

        self.restart()

        lines = self.execute_command("history").split('\n')

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        self.assertGreater(len(lines), 0)

        self.assertIn("1", lines[0])
        self.assertIn("echo foo", lines[0])
        self.assertIn("2", lines[1])
        self.assertIn("echo bar", lines[1])
        self.assertIn("3", lines[2])
        self.assertIn("echo baz", lines[2])

    def test_entire_command_is_displayed(self):
        """Tests that entire command (including arguments and '&') is displayed after loading"""

        self.execute_command("ls")
        self.execute_command("ls -l")
        self.execute_command("ls -a -l")
        self.execute_command("ls -l&")

        self.restart()

        lines = self.execute_command("history").split('\n')

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        self.assertGreater(len(lines), 0)

        self.assertRegexpMatches(lines[0], "ls\s*$")
        self.assertRegexpMatches(lines[1], "ls -l\s*$")
        self.assertRegexpMatches(lines[2], "ls -a -l\s*$")
        self.assertRegexpMatches(lines[3], "ls -l ?&\s*$")

    def test_large_history_size_is_preserved(self):
        """Tests that the size of the history buffer is preserved across restarts"""

        self.execute_command("sethistory 15")

        for i in range(20):
            self.execute_command("echo %d" % (i,))

        lines = self.execute_command("history").split('\n')

        self.assertGreater(len(lines), 0)

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        self.assertEqual(len(lines), 15)

        self.restart()

        lines = self.execute_command("history").split('\n')

        self.assertGreater(len(lines), 0)

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        self.assertEqual(len(lines), 15)

    def test_small_history_size_is_preserved(self):
        """Tests that the size of the history buffer is preserved across restarts"""

        self.execute_command("sethistory 5")

        for i in range(20):
            self.execute_command("echo %d" % (i,))

        lines = self.execute_command("history").split('\n')

        self.assertGreater(len(lines), 0)

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        self.assertEqual(len(lines), 5)

        self.restart()

        lines = self.execute_command("history").split('\n')

        self.assertGreater(len(lines), 0)

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        self.assertEqual(len(lines), 5)


class FeaturesAfterInputTestCase(ShellTestCase):

    def setUp(self):
        """Populates history and restarts the shell before tests are run"""

        super(FeaturesAfterInputTestCase, self).setUp()

        # Save output for comparison in tests
        self.ls = self.execute_command("ls")
        self.lsl = self.execute_command("ls -l")
        self.lsal = self.execute_command("ls -a -l")
        self.ls_bg = self.execute_command("ls&")
        self.lsl_bg = self.execute_command("ls -l&")
        self.lsal_bg = self.execute_command("ls -a -l&")
        self.pwd = self.execute_command("pwd")

        self.restart()

    def test_history(self):
        """Tests that the "history" command works properly after a restart"""

        lines = self.execute_command("history").split('\n')

        self.assertGreater(len(lines), 0)

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        for i in range(7):
            self.assertIn("%d" % (i + 1,), lines[i])

    def test_h(self):
        """Tests that the "h" command works properly after a restart"""

        lines = self.execute_command("h").split('\n')

        self.assertGreater(len(lines), 0)

        self.assertGreater(len(lines), 0)

        # Strip all but numbered lines
        lines = [line for line in lines if any(char.isdigit() for char in line)]

        for i in range(7):
            self.assertIn("%d" % (i + 1,), lines[i])

    def test_rr(self):
        """Tests that the "rr" command works properly after a restart"""

        output = self.execute_command("rr")

        self.assertIn(self.pwd, output)

    def test_rr_bg(self):
        """Tests that the "rr" command works properly for a command with "&" after a restart"""

        output_before = self.execute_command("pwd&")

        self.restart()

        output_after = self.execute_command("rr")

        self.assertIn(output_before, output_after)

    def test_r_num(self):
        """Tests that the "r num" command works properly after a restart"""

        output_pwd = self.execute_command("r 7")

        self.assertIn(self.pwd, output_pwd)

    def test_r_num_bg(self):
        """Tests that the "r num" command works properly for a command with "&" after a restart"""

        output_before = self.execute_command("pwd&")

        self.restart()

        output_after = self.execute_command("r 7")

        self.assertIn(output_before, output_after)

    def test_r_str(self):
        """Tests that the "r str" command works properly after a restart"""

        output = self.execute_command("r ls")

        self.assertIn(self.lsal_bg, output)


class ChangeDirectoryCommandTestCase(ShellTestCase):

    def get_shell_cwd(self):
        """Gets the shell's current working directory"""

        output = self.execute_command("pwd")

        if output.endswith("\n" + self.prompt):
            output = output[:-len("\n" + self.prompt)]

        output = output.strip()

        self.assertTrue(os.path.exists(output.strip()), msg="coult not get cwd")

        return output

    def test_cd_with_relative_path(self):
        """Tests that cd works with relative path"""

        self.assertEqual(os.getcwd(), self.get_shell_cwd())

        if not os.path.exists('foo'):
            os.mkdir('foo')

        self.execute_command("cd foo")

        self.assertEqual(self.get_shell_cwd(), os.path.join(os.getcwd(), 'foo'))

        if os.path.exists('foo'):
            os.rmdir('foo')

    def test_cd_with_absolute_path(self):
        """Tests that cd works with absolute path"""

        home = os.environ['HOME']

        self.execute_command("cd %s" % (home,))

        self.assertEqual(self.get_shell_cwd(), home)

    def test_cd_up_one_level(self):
        """Tests that cd works with the ".." alias"""

        old_cwd = self.get_shell_cwd()

        self.execute_command("cd ..")

        self.assertEqual(self.get_shell_cwd(), os.path.dirname(old_cwd))

    def test_cd_no_arguments(self):
        """Tests that cd with no arguments works"""

        self.execute_command("cd")

        self.assertEqual(self.get_shell_cwd(), os.environ['HOME'])


if __name__ == '__main__':
    unittest.main()

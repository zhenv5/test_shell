# README #

This repository contains a set of test cases for the shells implemented in Labs 1 and 2 in the OSU CSE 2431 Operating Systems course. These scripts are used to help grade submissions, and may be used by students to verify their solutions.

## Details

The tests are written using Python's unittest module and test the requirements outlined in the assignment rubrics.

## Requirements

The tests require that Python 2 is available. If you have access to pip, you might want to install cricket, which provides a nice user interface for running the tests.

## Usage

To test a single shell:

1. Compile the shell into a binary and move that binary into the root of this repository
2. Modify the configuration options at the top of test_shell.py and insert the name of your shell's binary file as well as the name of your shell's history file (if applicable)
3. To run all tests, simply execute test_shell.py. Or, if you have installed cricket, execute cricket-unittest to run the tests in a GUI

### For graders

This repository also contains a simple shell script that is useful for setting up a batch of submissions for grading. This script extracts an archive of submissions, makes an attempt to compile each submission, and symlinks test_shell.py to each submission subdirectory.

To correctly generate a submission archive, execute the following:


```
#!bash

cd /submit/c2431aX/grader/c2431aX
tar -czf ~/lab2_submissions.tar.gz lab2/
```

Then, copy the tarball into the root of this repository and run the setup.sh script.

After running the script, you will need to do the following to test each submission:

1. cd into the submission directory and verify that compilation took place. You may need to manually compile if, for example, there is a README or special instructions for compilation or if the source is kept in a subdirectory.
2. Determine the name of the history file by reading the student's source code and make the corresponding modification to test_shell.py. It usually isn't necessary to know whether the file is saved in $HOME or $PWD, because the test suite accounts for both possibilities.
3. Execute test_shell.py or (highly recommended) start cricket-unittest to run the test cases.
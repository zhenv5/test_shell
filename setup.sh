#!/usr/bin/env bash

# setup.sh

# Sets up and compiles students' submissions and links tests to the local directory


# Configuration

# Location of the archive containing submissions
submission_archive=lab2_submissions.tar.gz

# History regex
re="\"([^\"]+)\""


tar -xzf $submission_archive
cd lab2

for student in `ls`; do
  cd $student

  # Compile
  gcc -o shellC *.c
  if [[ $? -ne 0 ]]; then
    echo "Could not compile shellC for $student"
  fi

  # Link up the test suite
  ln -s ../../test_shell.py ./

  cd ..
done
